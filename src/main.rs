use rand::Rng;
use std::cmp::Ordering;
use std::fs::File;
use std::io;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Write;

fn main() {
	println!("Welcome to Vince's guessing game! Here are your high scores!!!");
	let path = String::from("scoreboard.txt");

	let file = match File::open(&path) {
		Ok(file) => file,
		Err(_) => {
			println!("Couldn't find scoreboard. Creating a new one...");
			let mut x = File::create(&path).expect("Error creating new scoreboard");
			for _i in 0..3 {
				x.write_all(("100\n").as_bytes()).expect("write failed");
			}
			x = File::open(&path).expect("Something very bad has happened. Get a priest.");
			x
		}
	};
	let reader = BufReader::new(file);

	let scoreboard: Vec<u32> = reader
		.lines()
		.map(|line| line.unwrap().parse::<u32>().unwrap())
		.collect();
	for (i, element) in scoreboard.iter().enumerate() {
		println!("{}: {}", i + 1, element);
	}

	println!("Guess the number! Guess a number between 1-100 (inclusive)");

	let secret_number = rand::thread_rng().gen_range(1..=100);
	let mut num_guesses = 0;

	loop {
		println!("Please input your guess.");

		let mut guess = String::new();

		io::stdin()
			.read_line(&mut guess)
			.expect("Failed to read line");

		let guess: u32 = match guess.trim().parse() {
			Ok(num) => num,
			Err(_) => {
				println!("Please enter a natural number. This wont count against your guesses.");
				continue;
			}
		};

		println!("You guessed: {}", guess);

		match guess.cmp(&secret_number) {
			Ordering::Less => {
				num_guesses += 1;
				println!("Too Small!");
			}
			Ordering::Greater => {
				num_guesses += 1;
				println!("Too Big!");
			}
			Ordering::Equal => {
				num_guesses += 1;
				if num_guesses == 0 {
					println!("You Win! You found the correct number after 1 try!");
				} else {
					println!(
						"You Win! You found the correct number after {} tries!",
						num_guesses
					);
				}
				if num_guesses < scoreboard[scoreboard.len() - 1] {
					let mut new_scoreboard = vec![scoreboard[0], scoreboard[1], num_guesses];
					new_scoreboard.sort();
					std::fs::remove_file(&path).expect("Error Updating Scoreboard");
					let mut file = File::create(&path).expect("Error updating scoreboard");
					for x in new_scoreboard {
						file.write_all((x.to_string() + "\n").as_bytes())
							.expect("write failed");
					}
				}
				break;
			}
		}
	}
}
